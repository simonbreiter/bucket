require('dotenv').config()

const express = require('express')
const { graphqlHTTP } = require('express-graphql')
const port = process.env.API_PORT
const { buildSchema } = require('../graphql/schema')
const { identifyUser } = require('../middleware/identifyUser')
const createRoutes = require('../routes')
const initializeDatabases = require('../db')

let app = express()

app.use(identifyUser)

module.exports = initializeDatabases().then(connection => {
  return createRoutes(app, connection)
})
