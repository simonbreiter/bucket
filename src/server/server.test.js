const request = require('supertest')
const createServer = require('./server')

test('simple request', done => {
  const expected = 'List of buckets'

  return createServer.then(server => {
    request(server.app)
      .get('/buckets')
      .then(response => {
        expect(response.text).toEqual(expected)
        server.connection.close()
        done()
      })
  })
})
