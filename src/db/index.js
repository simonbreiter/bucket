require('dotenv').config()

const MongoClient = require('mongodb').MongoClient
const url = `mongodb://myuser:example@${process.env.MONGODB_HOST}:27017`

module.exports = async function () {
  return MongoClient.connect(url)
}
