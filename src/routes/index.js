module.exports = function (app, connection) {
  // TODO: Use REST to create, update and delete buckets
  app.get('/buckets', (req, res) => {
    res.send('List of buckets')
  })

  // TODO: Use GraphQL to query buckets
  //   app.use(
  //     '/',
  //     graphqlHTTP(async (req, res) => {
  //       return {
  //         // build new schema on each request, so we can modify it at runtime
  //         schema: buildSchema(req.graphql),
  //         // TODO: Pass global mongoDbClient here
  //         // context: {
  //         //   connection: res.locals.connection,
  //         //   db: res.locals.db
  //         // },
  //         graphiql: true
  //       }
  //     })
  //   )

  return { app, connection }
}
